package com.prex.monitor.exception;

/**
 * @Classname RedisConnectException
 * @Description Redis 连接异常
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-02 14:53
 * @Version 1.0
 */
public class RedisConnectException extends Exception {

    private static final long serialVersionUID = 1639374111871115063L;

    public RedisConnectException(String message) {
        super(message);
    }
}

