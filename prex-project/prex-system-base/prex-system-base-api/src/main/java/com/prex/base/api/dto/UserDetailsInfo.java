package com.prex.base.api.dto;

import com.prex.base.api.entity.SysUser;
import lombok.Data;

import java.util.Set;

/**
 * @Classname UserInfo
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-08-15 11:17
 * @Version 1.0
 */
@Data
public class UserDetailsInfo {

    /**
     * 用户基本信息
     */
    private SysUser sysUser;

    /**
     * 用户权限
     */
    Set<String> permissions;
}
