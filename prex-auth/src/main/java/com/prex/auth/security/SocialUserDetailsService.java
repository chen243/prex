package com.prex.auth.security;

import com.prex.common.auth.service.LoginType;
import com.prex.common.auth.service.PrexSecurityUser;

/**
 * @Classname SocialUserDetailsService
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-16 13:47
 * @Version 1.0
 */
public interface SocialUserDetailsService {

    PrexSecurityUser loadUser(String para, LoginType loginType);
}
